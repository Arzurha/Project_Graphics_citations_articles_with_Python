#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Master Bio-informatique
# Auteur : EMERY Clara
# Projet de Python 

#Main execute all the function if you want to see the result just cancel the #

#Libraries
import xml.dom.minidom
import glob
import os.path
import os
import networkx as nx
 
def getAuthors (domArticle): 
	authorElts = domArticle.getElementsByTagName("Author")
	for currentAuthorElt in authorElts:
		authorLastNameValue = ""
		authorFirstNameValue = ""
# we assume that both elements
# - are present
# - are unique
# so getElementsByTagName(...) returns a list of 1 element
		authorLastNameList = currentAuthorElt.getElementsByTagName("LastName")
		authorFirstNameList = currentAuthorElt.getElementsByTagName("ForeName")
		if (authorLastNameList.length==1) and (authorFirstNameList.length==1):
			authorLastNameElt = authorLastNameList[0]
			authorFirstNameElt = authorFirstNameList[0]
# we assume that both elements only contain text
# (so they only have 1 child)
			authorLastNameValue = authorLastNameElt.childNodes[0].data
			authorFirstNameValue = authorFirstNameElt.childNodes[0].data
		return (authorLastNameValue, authorFirstNameValue)

#Question 1
#Access to PMID elt: 
def getFirstPMID (pmid) : 
	pmidElts = pmid.getElementsByTagName("PMID")
	pmidEltsValue= pmidElts[0].childNodes[0].data
	return pmidEltsValue


#Access to CommentsCorrections from CommentsCorrectionsList 
def getArticlesCitesby(FilePathdata_fichier, elements) :
	domArticle=xml.dom.minidom.parse(FilePathdata_fichier+elements+".xml")
	commElts = domArticle.getElementsByTagName("CommentsCorrections")
	CommPMID=[]
	for currentCommElt in commElts:	
		CommPMIDList=currentCommElt.getElementsByTagName("PMID")
		if (CommPMIDList.length!=0) :
			CommPMIDElt=CommPMIDList[0]
			CommPMID.append(CommPMIDElt.childNodes[0].data)
	return CommPMID

#Do a list of all corpus elt
def getListCorpus(FilePathdata) :
	listcorpus=[]
	articlesCorpus=[]
	listcorpus=(os.listdir(FilePathdata))
	for i in listcorpus : 
		articlesCorpus.append(i.replace(".xml",""))
	return articlesCorpus

def getIncrementationDict(FilePathdata_fichierxml, articlesCorpus):
	dictArticlesCites={}
	for i in range (0,len(articlesCorpus) ):
		dictArticlesCites[articlesCorpus[i]]=getArticlesCitesby(FilePathdata_fichierxml,articlesCorpus[i])
	return (dictArticlesCites)

def getArticlesInGph(articlesCorpus,dictArticlesCites):
	articlesInGraph=list(articlesCorpus)
	value=dictArticlesCites.values()
	for l in value :
		for elt in l :
				if elt not in articlesInGraph : 
					articlesInGraph.append(elt)
	return (articlesInGraph)

def	getArticleFromCorpusB(InGraph, Corpus) :
	articleIsOriginalCorpus={}
	for i in range(len(InGraph)):
		articleIsOriginalCorpus[InGraph[i]]= "FALSE"
		if InGraph[i] in Corpus :
			articleIsOriginalCorpus[InGraph[i]]="TRUE"
	return (articleIsOriginalCorpus)
	
def getMoreCitedBy(dic): 
	m=0
	for i in dic:
		if len(dic[i])>m :
			m=len(dic[i])
			pmid=i
	return (pmid,m)

def getInversedDict(articlesInGraph, dictArticlesCites) :
	dictArticlesCitedBy={}
	for i in range (len(articlesInGraph)):
		dictArticlesCitedBy[articlesInGraph[i]]=[] #On remplit d'abord le dictionnnaire avec les clefs du corpus

	for (key,value) in dictArticlesCites.items():
		for elt in value :
			if not key in dictArticlesCitedBy[elt] : 
				dictArticlesCitedBy[elt].append(key)
	return dictArticlesCitedBy

def main () :
	##All the path to different directory which are modifiable :
	FilePathdata="/home/clara/Documents/M1/Python/Tp/Projet/data" ##You must to adapt the way to reach the correct directory
	FilePathdata_fichierxml="/home/clara/Documents/M1/Python/Tp/Projet/data/notices-metabolicNetworkReconstruction/data/"
	FilePathdata_ListCorpus="/home/clara/Documents/M1/Python/Tp/Projet/data/notices-metabolicNetworkReconstruction/data"
	Filepathsrc="/home/clara/Documents/M1/Python/Tp/Projet/src"##FilePath where stand the programm

	##Article used for first questions (need to be parse before anyuse)
	domArticle=xml.dom.minidom.parse(FilePathdata_fichierxml+"17132165.xml")
	
	##Question 1
	authorLastNameValue,authorFirstNameValue=getAuthors(domArticle)
	#print(authorLastNameValue + "\t" + authorFirstNameValue)

	##Question 2 collected PMID value in each element CommentsCorrections from CommentsCorrectionsList
	pmid_art=getFirstPMID(domArticle)
	#print("First PMID ist : ",pmid_art)	

	##Question 3 listing from articles's PMID by PMID
	citation=[]
	elements="17132165"
	citation=getArticlesCitesby(FilePathdata_fichierxml,elements)
	#print(citation)

	##Question 4 PMID from corpus article
	articlesCorpus=[]
	articlesCorpus=getListCorpus(FilePathdata_ListCorpus)
	#print(len(articlesCorpus))
	#print(articlesCorpus)

	##Question 5
	dictArticlesCites=getIncrementationDict(FilePathdata_fichierxml, articlesCorpus)
	#print(len(dictArticlesCites))


	##Question 6 without doublon  
	articlesInGraph=getArticlesInGph(articlesCorpus, dictArticlesCites)
	#print(len(articlesInGraph))
	#print (articlesInGraph)
	

	##Question 7
	articleIsOriginalCorpus=getArticleFromCorpusB(articlesInGraph, articlesCorpus)
	#print(articleIsOriginalCorpus)


	##Question 8
	(moreCitedPmid,pmidTime)=getMoreCitedBy(dictArticlesCites)																							
	#print("l'article ",moreCitedPmid," est cite ",pmidTime," fois.") #23892182
	
	##Question 9 
	dictArticlesCitedBy=getInversedDict(articlesInGraph, dictArticlesCites)
	#print(dictArticlesCitedBy)	

	##Qestion 10
	pmidMoreCited=getMoreCitedBy(dictArticlesCitedBy)
	#print (pmidMoreCited)#20057383
	#print (pmidMoreCited in articlesCorpus)

	##Question 12
	#graph=GraphCitation()
	G=nx.DiGraph() # for a directed graph
	G_all=nx.DiGraph() #graph for all citations 
	#G=nx.Graph() # for an undirected graph
	#At first we build only the graph with only articlesCorpus and how they are related
	for i in articlesCorpus :
		G.add_node(i)
		for j in dictArticlesCites :
			if i in dictArticlesCites[j] :
				G.add_edge(i,j)

	#Then we generalise to all citation :
	for i in dictArticlesCitedBy :
		G_all.add_node(i)
		if i in articlesCorpus : 
			G_all.add_node(i,src="Article du Corpus")
		for j in dictArticlesCitedBy :
			if i in dictArticlesCitedBy[j] :
				G_all.add_edge(i,j)


	nx.write_gexf(G, "graph-citation.gexf", encoding='utf-8',prettyprint=True, version='1.1draft')
	nx.write_gexf(G_all, "graph-citation_allCited.gexf", encoding='utf-8',prettyprint=True, version='1.1draft')

main()
